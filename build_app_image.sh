#!/usr/bin/env zsh

export VERSION="v0.0.2"
export IMG="quay.io/rmegensgls/simple-webevents-app"

sudo podman build -t ${IMG}:${VERSION} -f ./Dockerfile